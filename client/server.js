
// variables required for starting server
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
var session = require('express-session');
const app = express()

// importing other node js functions
// var cmd=require('node-cmd');
// var sys = require('sys')
// var exec = require('child_process').exec;


// importing fabric clients
var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');
var path = require('path');
var util = require('util');
var os = require('os');

const favicon = require('express-favicon');

// instantiating fabric client and creating variables
var fabric_client = new Fabric_Client();
var fabric_ca_client = null;
var admin_user = null;
var member_user = null;
var store_path = path.join(__dirname, 'hfc-key-store');
console.log(' Store path:'+store_path);
var adminName;
var pass;


// setting engine and using static files

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')

app.use(favicon(__dirname + '/public/img/fevicon.png'));

var channel = fabric_client.newChannel('template');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);

var achannel = fabric_client.newChannel('admission');
var apeer = fabric_client.newPeer('grpc://localhost:7051');
achannel.addPeer(apeer);
var aorder = fabric_client.newOrderer('grpc://localhost:7050')
achannel.addOrderer(aorder);

var gchannel = fabric_client.newChannel('temporary');
var gpeer = fabric_client.newPeer('grpc://localhost:7051');
gchannel.addPeer(gpeer);
var gorder = fabric_client.newOrderer('grpc://localhost:7050')
gchannel.addOrderer(gorder);

var uchannel = fabric_client.newChannel('user');
var upeer = fabric_client.newPeer('grpc://localhost:7051');
uchannel.addPeer(upeer);
var uorder = fabric_client.newOrderer('grpc://localhost:7050')
uchannel.addOrderer(uorder);


var apchannel = fabric_client.newChannel('approval');
var appeer = fabric_client.newPeer('grpc://localhost:7051');
apchannel.addPeer(appeer);
var aporder = fabric_client.newOrderer('grpc://localhost:7050')
apchannel.addOrderer(aporder);


var dchannel = fabric_client.newChannel('degree');
var dpeer = fabric_client.newPeer('grpc://localhost:7051');
dchannel.addPeer(dpeer);
var dorder = fabric_client.newOrderer('grpc://localhost:7050')
dchannel.addOrderer(dorder);





app.use(session({
  secret: 'hyperledgerfabric',
  cookie: {},
  saveUninitialized: false,
  resave: false

}));


// app.get('/testcmd', function (req, res) {

// dir = exec('docker exec cli peer channel fetch newest user.block -c user -o orderer.example.com:7050', 
//   function(err, stdout, stderr) {
//   if (err) {
//    console.log(err); 
//   }
//   console.log(stdout);
//   res.send(stdout);
// });

// dir.on('exit', function (code) {
//   console.log(code);
// });

// })



app.get('/logout', function (req, res) {

 req.session.destroy(function(){
  res.redirect('/userlogin');
});

})



app.get('/profile', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  }

  var profileData = {username: req.session.username, permission: req.session.permission, active: req.session.active, by: req.session.by, approve: req.session.approve, userid: req.session.userid, speacs: req.session.speacs};
  res.render('profile',profileData);


})




app.post('/updatepassword', function (req, res) { 

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  }

  var password = req.body.current;
  var newpassword = req.body.new;

  var dataForUser = [];

  dataForUser[0] = req.session.userkey;
  dataForUser[1] = newpassword;


  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'changePassword',
    args: dataForUser,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return uchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = uchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer Update User Password');
    var profileData = {username: req.session.username, permission: req.session.permission, active: req.session.active, by: req.session.by, approve: req.session.approve, userid: req.session.userid,  speacs: req.session.speacs};
    res.render('profile',profileData);
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  res.send("Success Check Console")

  // res.send("FAILED TO INVOKE");

});





})









// route
app.get('/', function (req, res) {

  res.redirect('/enrolladmin');
  
})









app.get('/allusers', function (req, res) {

  // res.render('allusers',{username: req.session.username, permission: req.session.permission});


  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3") && !(req.session.permission == "2") && !(req.session.permission == "1") ) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }


  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'queryAllUsers',
    args: ['']
  };

  // send the query proposal to the peer
  return uchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending Template keys to page")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj, username: req.session.username, permission: req.session.permission};
    res.render('allusers', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});
  //end



  
})






app.post('/rejectuser', function (req, res) {

 var dataForUser = [];

 dataForUser[0] = req.body.userkey;



 console.log('Store path:'+store_path);
 var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'rejectUser',
    args: dataForUser,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return uchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = uchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.send("Successfully committed the change to the ledger by the peer Rejected")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.send("Error")

  // res.send("FAILED TO INVOKE");

});


})









app.get('/approveuser', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }


  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'queryAllUsers',
    args: ['']
  };

  // send the query proposal to the peer
  return uchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending Template keys to page")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj,message:"null", username: req.session.username, permission: req.session.permission};
    res.render('approveuser', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});
  //end
  
})





app.post('/approveuser', function (req, res) {

 var dataForUser = [];

 dataForUser[0] = req.body.userkey;
 


 console.log('Store path:'+store_path);
 var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'approveUser',
    args: dataForUser,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return uchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = uchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.send("Successfully committed the change to the ledger by the peer Approved")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.send("Success Check Console")

  // res.send("FAILED TO INVOKE");

});


})









app.post('/deleteuser', function (req, res) {

 var dataForUser = [];

 dataForUser[0] = req.body.userid;
 


 console.log('Store path:'+store_path);
 var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);


  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'deleteUser',
    args: dataForUser,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return uchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };


    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = uchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer DELETED User');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.redirect('/deleteuser');
    // res.send("Successfully committed the change to the ledger by the peer Approved")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.send("Success Check Console")

  // res.send("FAILED TO INVOKE");

});


})











app.get('/deleteuser', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "1")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }  


  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'queryAllUsers',
    args: ['']
  };

  // send the query proposal to the peer
  return uchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending users to page")
    
    // console.log(obj[0]["Key"])
    var dataToUser = {result: obj,message: "null", username: req.session.username, permission: req.session.permission};
    res.render('deleteuser', dataToUser);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});
  //end




})





app.get('/adduser', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3") && !(req.session.permission == "2") && !(req.session.permission == "1")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }  else {
   res.render('adduser',{message: "null", username: req.session.username, permission: req.session.permission})
 }


})


app.post('/adduser', function (req, res) {


 var dataForUser = [];

 dataForUser[0] = req.body.userid;
 dataForUser[1] = req.body.username;
 dataForUser[2] = req.body.password;
 dataForUser[3] = req.body.publickey;
 dataForUser[4] = req.body.active;
 dataForUser[5] = req.body.permission;
 dataForUser[6] = req.body.by;
 dataForUser[7] = "0";


 console.log('Store path:'+store_path);
 var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);


  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'addUser',
    args: dataForUser,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return uchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = uchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('adduser',infoMsg);
    // res.send("Successfully committed the change to the ledger by the peer")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('adduser',infoMsg);

  // res.send("FAILED TO INVOKE");

});




})







app.get('/verifydegree', function (req, res) {

  res.render('verifydegree',{degreeno: "", error:"0"});


})


app.post('/verifieddegree', function (req, res) {


  var qDeg = [];
  qDeg[0] = req.body.degreeno.toUpperCase();
  dataToDeg = { degresult: null, dno: req.body.degreeno, username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'queryDeg',
    args: qDeg
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());


      if( query_responses[0].toString() ) {
        
        var sresult = query_responses[0];
        var sstr = String.fromCharCode.apply(String, sresult)
        var stdObj = JSON.parse(sstr);
        dataToDeg.degresult = stdObj;

        res.render('verifieddegree',dataToDeg)

      } else {
        res.render('verifydegree',{degreeno: qDeg, error:"1"});
      }


    }
  } else {
    console.log("No payloads were returned from query");
  }
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})











app.post('/openapproveddegree', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var qDeg = [];
  qDeg[0] = req.body.degid;
  dataToDeg = { degresult: null, did: req.body.degid, username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'queryDeg',
    args: qDeg
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToDeg.degresult = stdObj;


      setTimeout(function afterTwoSeconds() {
        res.render('openapproveddegree',dataToDeg)
      }, 2000)

    }
  } else {
    console.log("No payloads were returned from query");
  }
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})









app.post('/degreeapprovebycontroller',function(req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "4")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var degid = req.body.degid;
  var controller = "1,"+req.body.user;

  var dataForDegree = [];
  
  dataForDegree[0] = degid;
  dataForDegree[1] = controller;




  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mydeg',
    fcn: 'controllerDeg',
    args: dataForDegree,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return dchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };


    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = dchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');


    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackdegreeapprovebycontroller',infoMsg);

    
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackdegreeapprovebycontroller',infoMsg);

  // res.send("FAILED TO INVOKE");

}); 



})







app.post('/openpublishdegree', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "4")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var qDeg = [];
  qDeg[0] = req.body.degid;
  dataToDeg = { degresult: null, did: req.body.degid, username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'queryDeg',
    args: qDeg
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToDeg.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('openpublishdegree',dataToDeg)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})












app.post('/opendegreecontroller', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "4")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var qDeg = [];
  qDeg[0] = req.body.degid;
  dataToDeg = { degresult: null, did: req.body.degid, username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'queryDeg',
    args: qDeg
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToDeg.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('opendegreecontroller',dataToDeg)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})














app.post('/opendegreeapproval', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var qDeg = [];
  qDeg[0] = req.body.degid;
  dataToDeg = { degresult: null, did: req.body.degid, username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'queryDeg',
    args: qDeg
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToDeg.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('opendegreeapproval',dataToDeg)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})








app.get('/approveddegree', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { degresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'listAllDegs',
    args: ['']
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('approveddegree',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})








app.get('/degreecontroller', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "4")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { degresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'listAllDegs',
    args: ['']
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('degreecontroller',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})









app.get('/publishdegrees', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "4")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { degresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'listAllDegs',
    args: ['']
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('publishdegrees',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})













app.get('/degreeapproval', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { degresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'listAllDegs',
    args: ['']
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('degreeapproval',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})







app.get('/createddegree', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { degresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mydeg',
    fcn: 'listAllDegs',
    args: ['']
  };

  // send the query proposal to the peer
  return dchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.degresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('createddegree',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})











app.post('/adddetails',function(req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var stdid = req.body.rollno;
  var user = req.body.user;
  var approver = "0";
  var controller = "0";
  // var semester = req.body.semester;

  var variables = req.body.Variables;
  var fields = req.body.Fields;
  var submarks = req.body.SubjectMarks;

  var dataForDegree = [];
  var stddetails = "";

  
  dataForDegree[0] = stdid;
  dataForDegree[1] = user;
  dataForDegree[2] = approver;
  dataForDegree[3] = controller;

  

  if(variables) {
    for (i = 0; i < variables.length; i++) { 
      stddetails += variables[i]+","+fields[i];
      if(i != (variables.length-1) ) {
        stddetails += "$";
      }
    }
  }

  dataForDegree[4] = stddetails;
  
  if(submarks) {
    for(i=0; i<submarks.length; i++) {
      dataForDegree[i+5] = submarks[i];
    }
  }





  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);


  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mydeg',
    fcn: 'createDeg',
    args: dataForDegree,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return dchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = dchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackadddetails',infoMsg);

    
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackadddetails',infoMsg);

  // res.send("FAILED TO INVOKE");

}); 





})

















app.post('/createdegree',function(req, res) { 
  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }
  
  dataToDeg = { stddata: null, stdresult: null,rn:req.body.rollno,username: req.session.username, permission: req.session.permission }

  var getData = []
  getData[0] = req.body.rollno
  // res.render('viewresult', {username: req.session.username, permission: req.session.permission})

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myrs',
    fcn: 'queryResult',
    args: getData
  };

  // send the query proposal to the peer
  return gchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var sre = JSON.parse(sstr);
      dataToDeg.stdresult = sre;
      

      console.log("Successfully Fetched Result and Now Fetching Student Data")





      var member_user = null;
      var store_path = path.join(__dirname, 'hfc-key-store');
      console.log('Store path:'+store_path);
      var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myacc',
    fcn: 'queryStudent',
    args: getData
  };

  // send the query proposal to the peer
  return achannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var data = query_responses[0];
      var sstr = String.fromCharCode.apply(String, data)
      var sdata = JSON.parse(sstr);
      dataToDeg.stddata = sdata;
      

      setTimeout(function afterTwoSeconds() {
        res.render('createdegree',dataToDeg)
      }, 2000)

    }
  } else {
    console.log("No payloads were returned from query");
  }
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})












}
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})











app.get('/adddegree', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { stdresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myrs',
    fcn: 'listAllResults',
    args: ['']
  };

  // send the query proposal to the peer
  return gchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.stdresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('adddegree',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})










app.post('/viewapprovedresult',function(req, res) { 
  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }
  
  dataToGrade = { result: null,rn:req.body.rollno,username: req.session.username, permission: req.session.permission }

  var getData = []
  getData[0] = req.body.rollno
  // res.render('viewresult', {username: req.session.username, permission: req.session.permission})

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myrs',
    fcn: 'queryResult',
    args: getData
  };

  // send the query proposal to the peer
  return gchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.result = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('viewapprovedresult',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})









app.post('/resubmitresult',function(req, res) { 
  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }
  
  dataToGrade = { result: null,rn:req.body.rollno,username: req.session.username, permission: req.session.permission }

  var getData = []
  getData[0] = req.body.rollno
  // res.render('viewresult', {username: req.session.username, permission: req.session.permission})

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'aqueryResult',
    args: getData
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.result = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('resubmitresult',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})









app.post('/viewresult',function(req, res) { 
  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }
  
  dataToGrade = { result: null,rn:req.body.rollno,username: req.session.username, permission: req.session.permission }

  var getData = []
  getData[0] = req.body.rollno
  // res.render('viewresult', {username: req.session.username, permission: req.session.permission})

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'aqueryResult',
    args: getData
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.result = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('viewresult',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})







app.get('/approvedresults', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { stdresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myrs',
    fcn: 'listAllResults',
    args: ['']
  };

  // send the query proposal to the peer
  return gchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.stdresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('approvedresults',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})







app.get('/allgrades', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { stdresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'alistAllResults',
    args: ['']
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.stdresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('allgrades',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})





})








app.get('/rejectresults', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToReject = { stdresult: null,username: req.session.username, permission: req.session.permission }




  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }


  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'alistAllResults',
    args: ['']
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToReject.stdresult = stdObj;
      

      setTimeout(function afterTwoSeconds() {
        res.render('rejectresults',dataToReject)
      }, 2000)

    }
  } else {
    console.log("No payloads were returned from query");
  }
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})





})












app.post('/approvedegree',function(req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var degid = req.body.degid;
  var approver = "1,"+req.body.user;

  var dataForDegree = [];
  
  dataForDegree[0] = degid;
  dataForDegree[1] = approver;




  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mydeg',
    fcn: 'approveDeg',
    args: dataForDegree,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return dchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = dchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackapprovedegree',infoMsg);

    
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackapprovedegree',infoMsg);

  // res.send("FAILED TO INVOKE");

}); 





})












app.post('/openresultapproval',function(req, res) { 
  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "2")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }
  
  dataToGrade = { result: null,rn:req.body.rollno,username: req.session.username, permission: req.session.permission }

  var getData = []
  getData[0] = req.body.rollno
  // res.render('viewresult', {username: req.session.username, permission: req.session.permission})

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'aqueryResult',
    args: getData
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.result = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('openresultapproval',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})







app.get('/resultapproval', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } //else if(!(req.session.permission == "3")) {
  //   res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  // } 

  // var channel = fabric_client.newChannel('template');
// var peer = fabric_client.newPeer('grpc://localhost:7051');
// channel.addPeer(peer);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myap',
    fcn: 'alistAllResults',
    args: ['']
  };

  // send the query proposal to the peer
  return apchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending Template keys to page")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj, username: req.session.username, permission: req.session.permission};
    res.render('resultapproval', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});
  //end
  
})








//Adding Approved REsult to grade channel
app.post('/savegrade',function(req, res) {
  console.log(req.body.reject+" - "+req.body.approve)

  if(req.body.reject == "Reject") {


    var stdid = req.body.key;
    var userid = req.body.userid;
    var rejectmsg = req.body.rejectmsg;

    var subjects = req.body.Subjects;


    var dataForRej = [];


    dataForRej[0] = stdid;
    dataForRej[1] = "2,"+rejectmsg; //status + Message;
    dataForRej[2] = userid;



    if(subjects) {
      for (i = 0; i < subjects.length; i++) { 
        dataForRej[i+3] = subjects[i];
      }
    }


    console.log('Store path:'+store_path);
    var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);


  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myap',
    fcn: 'arejectResult',
    args: dataForRej,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return apchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = apchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackresultreject',infoMsg);

    // res.send("Successfully committed the change to the ledger by the peer")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackresultreject',infoMsg);

  // res.send("FAILED TO INVOKE");

}); 




}
else if(req.body.approve == "Approve") {

  var stdid = req.body.key;
  var userid = req.body.userid;
  // var semester = req.body.semester;
  console.log(req.body.approve);

  var subjects = req.body.Subjects;
  

  var dataForSem = [];
  var dataToDel = [];
  
  dataToDel[0] = stdid;
  
  dataForSem[0] = stdid;
  dataForSem[1] = userid;

  

  if(subjects) {
    for (i = 0; i < subjects.length; i++) { 
      dataForSem[i+2] = subjects[i];
    }
  }
  


// res.send(dataForSem);


console.log('Store path:'+store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myrs',
    fcn: 'addSemesterResult',
    args: dataForSem,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return gchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = gchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    // res.redirect('/resultapproval');

    // Start Delete


    var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myap',
    fcn: 'adeleteResult',
    args: dataToDel,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return apchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = apchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackresultapproval',infoMsg);

    // res.send("Successfully committed the change to the ledger by the peer")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  console.log("delete Failed....");
  res.render('ackresultapproval',infoMsg);


}); 


    //End Delete
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  console.log("delete Failed....");
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackresultapproval',infoMsg);

}); 


} //if close


})








app.post('/updateresult',function(req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } //else if(!(req.session.permission == "3")) {
  //   res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  // }

  var stdid = req.body.stdid;
  var status = "0";
  var userid = req.body.userid;
  // var semester = req.body.semester;

  var variables = req.body.Variables;
  var marks = req.body.Marks;

  var dataForSem = [];
  var varval = "";
  // var tempKey = "TEMPLATE-"+batch.toUpperCase()+"-"+level.toUpperCase()+"-"+depart.toUpperCase();
  
  dataForSem[0] = stdid;
  dataForSem[1] = status;
  dataForSem[2] = userid;
  

  if(marks) {
    for (i = 0; i < marks.length; i++) { 
      var submark = variables[i]+","+marks[i];
      dataForSem[i+3] = submark;
    }
  }
  


// res.send(dataForSem);


console.log('Store path:'+store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myap',
    fcn: 'aaddSemesterResult',
    args: dataForSem,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return apchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = apchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackupdateresult',infoMsg);

    // res.send("Successfully committed the change to the ledger by the peer")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackupdateresult',infoMsg);

  // res.send("FAILED TO INVOKE");

}); 

})














app.post('/saveresult',function(req, res) {

  var stdid = req.body.stdid;
  var status = "0";
  var userid = req.body.userid;
  // var semester = req.body.semester;

  var variables = req.body.Variable;
  var marks = req.body.Marks;

  var dataForSem = [];
  var varval = "";
  // var tempKey = "TEMPLATE-"+batch.toUpperCase()+"-"+level.toUpperCase()+"-"+depart.toUpperCase();
  
  dataForSem[0] = stdid;
  dataForSem[1] = status;
  dataForSem[2] = userid;
  

  if(marks) {
    for (i = 0; i < marks.length; i++) { 
      var submark = variables[i]+","+marks[i];
      dataForSem[i+3] = submark;
    }
  }
  


// res.send(dataForSem);


console.log('Store path:'+store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myap',
    fcn: 'aaddSemesterResult',
    args: dataForSem,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return apchannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = apchannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackgrade',infoMsg);
    // res.redirect('/grade');

    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackgrade',infoMsg);

}); 

})











app.get('/grade', async function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  dataToGrade = { tempresult: "", stdresult: null,message:"null", username: req.session.username, permission: req.session.permission }

  console.log("testing here "+dataToGrade.stdresult);
  // setup the fabric network

  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'listAllTemplates',
    args: ['']
  };

  // send the query proposal to the peer
  return channel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var tresult = query_responses[0];
      var tstr = String.fromCharCode.apply(String, tresult);
      var tempObj = JSON.parse(tstr);
      dataToGrade.tempresult = tempObj;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page")
    // dataToGrade.stdresult = queryStd(achannel);

    

    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log('Store path:'+store_path);
    var tx_id = null;
  // var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myacc',
    fcn: 'listAllStudents',
    args: ['']
  };

  // send the query proposal to the peer
  return achannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      var sresult = query_responses[0];
      var sstr = String.fromCharCode.apply(String, sresult)
      var stdObj = JSON.parse(sstr);
      dataToGrade.stdresult = stdObj;
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    
    console.log("///end student data ///")
    setTimeout(function afterTwoSeconds() {
      res.render('grade',dataToGrade)
    }, 2000)

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



}
} else {
  console.log("No payloads were returned from query");
}
// res.status(200).json({response: query_responses[0].toString()});

}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



})


function  queryStd(mychannel) {
  // body
  var member_user = null;
  var store_path = path.join(__dirname, 'hfc-key-store');
  console.log('Store path:'+store_path);
  var tx_id = null;
  var sresult = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myacc',
    fcn: 'listAllStudents',
    args: ['']
  };

  // send the query proposal to the peer
  return mychannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());

      sresult = query_responses[0].toString();
      
      // return sresult;
    // console.log(ex[1].record["abc"])
    console.log("Sending Templates to page funtion return")
    console.log(sresult)
    console.log("///end student data ///")
    return sresult;

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
})



}






app.get('/dashboard', function (req, res) {
  if(req.session.username){
    res.render('dashboard',{username: req.session.username, permission: req.session.permission});
  } else {
    res.render('admin',{loginmsg: "0"})
  }
})








app.get('/alladmissions', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  // var channel = fabric_client.newChannel('template');
// var peer = fabric_client.newPeer('grpc://localhost:7051');
// channel.addPeer(peer);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'myacc',
    fcn: 'listAllStudents',
    args: ['']
  };

  // send the query proposal to the peer
  return achannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending Template keys to page")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj, username: req.session.username, permission: req.session.permission};
    res.render('alladmissions', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
  res.send("FAILED TO FETCH STUDENT LIST");
});
  //end
  
})












app.get('/admission', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var tx_id = null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'listAllTemplates',
    args: ['']
  };

  // send the query proposal to the peer
  return channel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
      
    // console.log(ex[1].record["abc"])
    console.log("start from here")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj, message: "null", username: req.session.username, permission: req.session.permission};
    res.render('admission', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
  res.send("FAILED TO FETCH KEYS");
});
  //end

})








//Admission Post
app.post('/admission',function(req, res) {

  var tk = req.body.tempkey;

  var variables = req.body.Variable;
  var values = req.body.Value;

  var dataForAdm = [];
  var varval = "";
  // var tempKey = "TEMPLATE-"+batch.toUpperCase()+"-"+level.toUpperCase()+"-"+depart.toUpperCase();
  

  dataForAdm[1] = "Template Key,"+tk;
  
  if(variables) {
    for (i = 0; i < variables.length; i++) { 
      if(variables[i] == "student id") {
        dataForAdm[0] = values[i]
      }else{
        varval = variables[i]+","+values[i];
        dataForAdm[i+1] = varval;
      }
    }
  }


  console.log('Store path:'+store_path);
  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'myacc',
    fcn: 'addStudent',
    args: dataForAdm,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return achannel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = achannel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var infoMsg = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('ackadmission',infoMsg);
    // res.send("Successfully committed the change to the ledger by the peer")
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var infoMsg = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('ackadmission',infoMsg);
  // res.send("FAILED TO INVOKE");
});

})










app.get('/alltemplate', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  } 

  // var channel = fabric_client.newChannel('template');
// var peer = fabric_client.newPeer('grpc://localhost:7051');
// channel.addPeer(peer);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'listAllTemplates',
    args: ['']
  };

  // send the query proposal to the peer
  return channel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Sending Template keys to page")
    
    // console.log(obj[0]["Key"])
    var dataToSend = {result: obj, username: req.session.username, permission: req.session.permission};
    res.render('alltemplate', dataToSend);

  }
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});
  //end
  
})








app.get('/template', function (req, res) {

  if(!(req.session.username)) {
    res.redirect('/userlogin');
  } else if(!(req.session.permission == "3")) {
    res.render("accessdenied",{username: req.session.username, permission: req.session.permission})
  }

  var dataToTemplate = {message: "null", username: req.session.username, permission: req.session.permission};
  res.render('template',dataToTemplate);
  
})







app.post('/template',function(req, res) {

  var depart = req.body.depart;
  var level = req.body.level;
  var batch = req.body.batch;

  var fields = req.body.Field;

  var scode = req.body.scode;
  var stitle = req.body.sname;
  var scredit = req.body.scredit;

  var subjects = [];
  var csfields = "";
  var tempKey = "TEMPLATE-"+batch.toUpperCase()+"-"+level.toUpperCase()+"-"+depart.toUpperCase();
  var dataTosend = []

  dataTosend[0] = tempKey;
  
  if(fields) {
    for (i = 0; i < fields.length; i++) { 
      csfields += fields[i];
      if( i != (fields.length-1)){
        csfields += ",";
      }
    }
  }

  dataTosend[1] = csfields;

  if(scode) {
    for (i = 0; i < scode.length; i++) { 
      subjects[i] = scode[i]+","+stitle[i]+","+scredit[i];
      dataTosend[i+2] = subjects[i];
    }
  }
  var myresult = " |\| U LL "

//invoking create template

//
console.log('Store path:'+store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // get a transaction id object based on the current user assigned to fabric client
  tx_id = fabric_client.newTransactionID();
  console.log("Assigning transaction_id: ", tx_id._transaction_id);

  // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
  // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
  // must send the proposal to endorsing peers
  var request = {
    //targets: let default to the peer assigned to the client
    chaincodeId: 'mycc',
    fcn: 'createTemplate',
    args: dataTosend,
    chainId: '',
    txId: tx_id
  };

  // send the transaction proposal to the peers
  return channel.sendTransactionProposal(request);
}).then((results) => {
  var proposalResponses = results[0];
  var proposal = results[1];
  let isProposalGood = false;
  if (proposalResponses && proposalResponses[0].response &&
    proposalResponses[0].response.status === 200) {
    isProposalGood = true;
  console.log('Transaction proposal was good');
} else {
  console.error('Transaction proposal was bad');
}
if (isProposalGood) {
  console.log(util.format(
    'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
    proposalResponses[0].response.status, proposalResponses[0].response.message));

    // build up the request for the orderer to have the transaction committed
    var request = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };

    // set the transaction listener and set a timeout of 30 sec
    // if the transaction did not get committed within the timeout period,
    // report a TIMEOUT status
    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
    var promises = [];

    var sendPromise = channel.sendTransaction(request);
    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

    // get an eventhub once the fabric client has a user assigned. The user
    // is required bacause the event registration must be signed
    let event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');

    // using resolve the promise so that result status may be processed
    // under the then clause rather than having the catch clause process
    // the status
    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        event_hub.disconnect();
        resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
      }, 3000);
      event_hub.connect();
      event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
        // this is the callback for transaction event status
        // first some clean up of event listener
        clearTimeout(handle);
        event_hub.unregisterTxEvent(transaction_id_string);
        event_hub.disconnect();

        // now let the application know what happened
        var return_status = {event_status : code, tx_id : transaction_id_string};
        if (code !== 'VALID') {
          console.error('The transaction was invalid, code = ' + code);
          resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
        } else {
          console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
          resolve(return_status);
        }
      }, (err) => {
        //this is the callback if something goes wrong with the event registration or processing
        reject(new Error('There was a problem with the eventhub ::'+err));
      });
    });
    promises.push(txPromise);

    return Promise.all(promises);
  } else {
    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
  }
}).then((results) => {
  console.log('Send transaction promise and event listener promise have completed');
  // check the results in the order the promises were added to the promise all list
  if (results && results[0] && results[0].status === 'SUCCESS') {
    console.log('Successfully sent transaction to the orderer.');
  } else {
    console.error('Failed to order the transaction. Error code: ' + results[0].status);
  }

  if(results && results[1] && results[1].event_status === 'VALID') {
    console.log('Successfully committed the change to the ledger by the peer');
    var dataToTemplate = {message: "1", username: req.session.username, permission: req.session.permission}
    res.render('template',dataToTemplate);
    
  } else {
    console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
  }
}).catch((err) => {
  console.error('Failed to invoke successfully :: ' + err);
  
  var dataToTemplate = {message: "0", username: req.session.username, permission: req.session.permission}
  res.render('template',dataToTemplate);
});


console.log(myresult)

})



app.post('/registeruser',function(req, res) {
  var users = req.body.user;


  Fabric_Client.newDefaultKeyValueStore({ path: store_path
  }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    var	tlsOptions = {
    	trustedRoots: [],
    	verify: false
    };
    // be sure to change the http to https when the CA is running TLS enabled
    fabric_ca_client = new Fabric_CA_Client('http://localhost:7054', null , '', crypto_suite);

    // first check to see if the admin is already enrolled
    return fabric_client.getUserContext('admin', true);
  }).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
      console.log('Successfully loaded admin from persistence');
      admin_user = user_from_store;
    } else {
      throw new Error('Failed to get admin.... run enrollAdmin.js');
    }

    // at this point we should have the admin user
    // first need to register the user with the CA server
    return fabric_ca_client.register({enrollmentID: users, affiliation: 'org1.department1',role: 'client'}, admin_user);
  }).then((secret) => {
    // next we need to enroll the user with CA server
    console.log('Successfully registered user1 - secret:'+ secret);

    return fabric_ca_client.enroll({enrollmentID: users, enrollmentSecret: secret});
  }).then((enrollment) => {
    console.log('Successfully enrolled member user "user1" ');
    return fabric_client.createUser(
     {username: users,
       mspid: 'SSUETMSP',
       cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
     });
  }).then((user) => {
   member_user = user;

   return fabric_client.setUserContext(member_user);
 }).then(()=>{
   console.log('User1 was successfully registered and enrolled and is ready to interact with the fabric network');
   res.render('dashboard');

 }).catch((err) => {
  console.error('Failed to register: ' + err);
  if(err.toString().indexOf('Authorization') > -1) {
    console.error('Authorization failures may be caused by having admin credentials from a previous CA instance.\n' +
      'Try again after deleting the contents of the store directory '+store_path);
  }
});


})




app.get('/userlogin', function (req, res) {

  if(req.session.username) {
    res.render('dashboard',{username: req.session.username, permission: req.session.permission})
  } else {
    res.render('admin',{loginmsg: "0"});
  }
  
})




app.post('/userlogin', function(req, res) {

  cuser = req.body.username.trim();
  cpass = req.body.password.trim();


  var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({ path: store_path
}).then((state_store) => {
  // assign the store to the fabric client
  fabric_client.setStateStore(state_store);
  var crypto_suite = Fabric_Client.newCryptoSuite();
  // use the same location for the state store (where the users' certificate are kept)
  // and the crypto store (where the users' keys are kept)
  var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
  crypto_suite.setCryptoKeyStore(crypto_store);
  fabric_client.setCryptoSuite(crypto_suite);

  // get the enrolled user from persistence, this user will sign all requests
  return fabric_client.getUserContext('admin', true);
}).then((user_from_store) => {
  if (user_from_store && user_from_store.isEnrolled()) {
    console.log('Successfully loaded user1 from persistence');
    member_user = user_from_store;
  } else {
    throw new Error('Failed to get user1.... run registerUser.js');
  }

  // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
  // queryAllCars chaincode function - requires no arguments , ex: args: [''],
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: 'mycc',
    fcn: 'queryAllUsers',
    args: ['']
  };

  // send the query proposal to the peer
  return uchannel.queryByChaincode(request);
}).then((query_responses) => {
  console.log("Query has completed, checking results");
  // query_responses could have more than one  results if there multiple peers were used as targets
  if (query_responses && query_responses.length == 1) {
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    } else {
      console.log("Response is ", query_responses[0].toString());
      var result = query_responses[0];
      var str = String.fromCharCode.apply(String, result);
      var obj = JSON.parse(str);
      
    // console.log(ex[1].record["abc"])
    console.log("Users Fetch validating credentials "+obj.length)
    var errmsg = "";
    breakme: {
     for(i=0;i<obj.length;i++) {
      if(cuser == obj[i].Record.username) {
        if(cpass == obj[i].Record.password) {
          if(obj[i].Record.approve == "1") {
            if( obj[i].Record.active == true) {
              req.session.username = obj[i].Record.username;
              req.session.permission = obj[i].Record.permission;
              req.session.userid = obj[i].Record.userid;
              req.session.active = obj[i].Record.active;
              req.session.by = obj[i].Record.by;
              req.session.approve = obj[i].Record.approve;
              req.session.userkey = obj[i].Key;
              req.session.speacs = obj[i].Record.password;

              res.render('dashboard',{username: req.session.username, permission: req.session.permission})
              break breakme;
            } else {
              console.log("User Not Active");
              errmsg = "3";
              res.render('admin',{loginmsg: errmsg})
              break breakme;
            }
          } else {
            console.log("User not Approved");
            errmsg = "2";
            res.render('admin',{loginmsg: errmsg})
            break breakme;
          }

        } else {
          console.log("this is incorrect pass");
          errmsg = "1";
        }

      } else {
        console.log("this is incorrect user");
        errmsg = "1";
      }

    }
    res.render('admin',{loginmsg: errmsg})
  }






}
} else {
  console.log("No payloads were returned from query");
}
}).catch((err) => {
  console.error('Failed to query successfully :: ' + err);
});



});






app.get('/enrolladmin', function(req, res) {
  var admins = "admin";
  var adminspw = "adminpw";
  var tem = ''
  if (admins === 'admin') {
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    var	tlsOptions = {
    	trustedRoots: [],
    	verify: false
    };
    // be sure to change the http to https when the CA is running TLS enabled
    fabric_ca_client = new Fabric_CA_Client('http://localhost:7054', tlsOptions , 'ca.example.com', crypto_suite);

    // first check to see if the admin is already enrolled
    return fabric_client.getUserContext(admins, true);
  }).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
      console.log('Successfully loaded admin from persistence');

      admin_user = user_from_store;
      return null;
    } else {
        // need to enroll it with CA server
        return fabric_ca_client.enroll({
          enrollmentID: admins,
          enrollmentSecret: adminspw
        }).then((enrollment) => {
          console.log('Successfully enrolled admin user "admin"');

          return fabric_client.createUser(
            {username: admins,
              mspid: 'SSUETMSP',
              cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
            });
        }).then((user) => {
          admin_user = user;
          return fabric_client.setUserContext(admin_user);
        }).catch((err) => {
          console.error('Failed to enroll and persist admin. Error: ' + err.stack ? err.stack : err);

          throw new Error('Failed to enroll admin');
        });
      }
    }).then(() => {
      console.log('Assigned the admin user to the fabric client ::' + admin_user.toString());

    }).catch((err) => {
      console.error('Failed to enroll admin: ' + err);
    }); 

    res.redirect("/userlogin");
  }
  else {
    console.log('Admin Enroll Error');
    res.redirect("/userlogin");
  }

});


// creating server
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')

})


