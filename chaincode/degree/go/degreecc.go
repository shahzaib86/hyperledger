/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"math/rand"
	"time"
	
	

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

// Define the car structure, with 4 properties.  Structure tags are used by encoding/json library



/*
 * The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "listAllDegs" {
		return s.listAllDegs(APIstub)
	} else if function == "createDeg" {
		return s.createDeg(APIstub, args)
	} else if function == "queryDeg" {
		return s.queryDeg(APIstub, args)
	} else if function == "approveDeg" {
		return s.approveDeg(APIstub, args)
	} else if function == "controllerDeg" {
		return s.controllerDeg(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}



func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	
	return shim.Success(nil)
}



func (s *SmartContract) listAllDegs(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := ""
	endKey := ""

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- List All Templates:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}



func (s *SmartContract) queryDeg(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	degAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(degAsBytes)
}

func (s *SmartContract) createDeg(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	degree := make(map[string]string)
	startKey := ""
	endKey := ""	
	check := true

	counter, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer counter.Close()
	i := 0
	for counter.HasNext() {
		t, _ := counter.Next()
		fmt.Println(t)
		i++
	}
	
	
	batch := strings.Split(args[0],"-")
	b := int64(19)
	if(batch[1] == "MS") {
		b = int64(30)
	}

	total := int(i+1)
	low := total
	hi := 999999    
	rand.Seed(time.Now().UnixNano())	
	ans := low + rand.Intn(hi-low)	
	
	degID := "DEGREE-"+batch[0][2:4]+strconv.FormatInt(int64(ans),10)+strconv.FormatInt(b,10)
 	
	
	ctr, er := APIstub.GetStateByRange(startKey, endKey)
	if er != nil {
		return shim.Error(er.Error())
	}
	defer ctr.Close()
	j := 0
	for ctr.HasNext() {
		x, _ := ctr.Next()
		if(x.Key == degID) {
			check = false	
			break	
		}
		fmt.Println(x)
		j++
	}	

	if(check) {
		
		degree["roll no"] = args[0]
		degree["user"] = args[1]
		degree["approver"] = args[2]
		degree["controller"] = args[3]
	 

		fields := strings.Split(args[4],"$")
		k := 0
		for k < len(fields) {
			fmt.Println("i is ", k)
			values := strings.Split(fields[k],",")
			vn := values[0]
			degree[vn] = values[1]
			k++
		}

	
		if(len(args) > 5) {

		l := 5
		for l < len(args) {
			fmt.Println("l is ", l)
			xx := l-4
			sno := strconv.Itoa(xx)
			sn := "subject"+sno
			degree[sn] = args[l]
			l++
		}

		}


		degAsBytes, _ := json.Marshal(degree)
	
		APIstub.PutState(degID, degAsBytes)

		return shim.Success(nil)

	} else {
		return shim.Error("Degree Already Exists")	
		
	}
}



func (s *SmartContract) approveDeg(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	degAsBytes, _ := APIstub.GetState(args[0])
	var degree  = make(map[string]string)

	json.Unmarshal(degAsBytes, &degree)
	degree["approver"] = args[1]

	degAsBytes, _ = json.Marshal(degree)
	APIstub.PutState(args[0], degAsBytes)

	return shim.Success(nil)
}


func (s *SmartContract) controllerDeg(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	degAsBytes, _ := APIstub.GetState(args[0])
	var degree  = make(map[string]string)

	json.Unmarshal(degAsBytes, &degree)
	degree["controller"] = args[1]

	degAsBytes, _ = json.Marshal(degree)
	APIstub.PutState(args[0], degAsBytes)

	return shim.Success(nil)
}



// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
