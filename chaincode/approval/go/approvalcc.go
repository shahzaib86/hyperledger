/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	
	

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

// Define the car structure, with 4 properties.  Structure tags are used by encoding/json library



/*
 * The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "alistAllResults" {
		return s.alistAllResults(APIstub)
	} else if function == "aaddSemesterResult" {
		return s.aaddSemesterResult(APIstub, args)
	} else if function == "aqueryResult" {
		return s.aqueryResult(APIstub, args)
	} else if function == "adeleteResult" {
		return s.adeleteResult(APIstub, args)
	} else if function == "arejectResult" {
		return s.arejectResult(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}



func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	/*	
	template := make(map[string]string)
	
	template["fields"] = "field,field,field"
	
	template["subject1"] = "SWE-101,CCN,4"
	template["subject2"] = "SWE-402,DSA,4"
	template["subject3"] = "SWE-301,ISE,5"
	

	temAsBytes, _ := json.Marshal(template)
	
	APIstub.PutState("TEMPLATE-2000-BS-CE", temAsBytes) */


	return shim.Success(nil)
}



func (s *SmartContract) alistAllResults(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := ""
	endKey := ""

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- List All Templates:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}




func (s *SmartContract) aaddSemesterResult(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	
	semresult := make(map[string]string)
	
		
	
/*	check := true
	startKey := ""
	endKey := ""
	counter, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer counter.Close()
	i := 0
	for counter.HasNext() {
		t, _ := counter.Next()
		if(t.Key == args[0]) {
			check = false	
			break	
		}
		fmt.Println(t)
		i++
	}
	*/
	semresult["status"] = args[1]
	semresult["userid"] = args[2]
	

	if(len(args) > 3) {
	i := 3

	for i < len(args) {
		fmt.Println("i is ", i)	
		sub := args[i]
		sm := strings.Split(sub, ",")
		submarks := sm[len(sm)-1]
		subname := sm[0]+","+sm[1]+","+sm[2]
		semresult[subname] = submarks
		i++
	}
	}

	srAsBytes, _ := json.Marshal(semresult)
	
	APIstub.PutState(args[0], srAsBytes)

	return shim.Success(nil)

}

func (s *SmartContract) aqueryResult(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	resultAsByte, _ := APIstub.GetState(args[0])
	return shim.Success(resultAsByte)
}


func (s *SmartContract) adeleteResult(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	
	
	err := APIstub.DelState(args[0])

	if err != nil {
		return shim.Error(err.Error())
	}
	
	return shim.Success(nil)
	
}




func (s *SmartContract) arejectResult(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	
	semresult := make(map[string]string)
	
		
	
/*	check := true
	startKey := ""
	endKey := ""
	counter, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer counter.Close()
	i := 0
	for counter.HasNext() {
		t, _ := counter.Next()
		if(t.Key == args[0]) {
			check = false	
			break	
		}
		fmt.Println(t)
		i++
	}
	*/
	semresult["status"] = args[1]
	semresult["userid"] = args[2]
	

	if(len(args) > 3) {
	i := 3

	for i < len(args) {
		fmt.Println("i is ", i)	
		sub := args[i]
		sm := strings.Split(sub, ",")
		submarks := sm[len(sm)-1]
		subname := sm[0]+","+sm[1]+","+sm[2]
		semresult[subname] = submarks
		i++
	}
	}

	srAsBytes, _ := json.Marshal(semresult)
	
	APIstub.PutState(args[0], srAsBytes)

	return shim.Success(nil)

}





// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
