#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
LANGUAGE=${1:-"golang"}
CC_SRC_PATH=github.com/template/go
if [ "$LANGUAGE" = "node" -o "$LANGUAGE" = "NODE" ]; then
	CC_SRC_PATH=/opt/gopath/src/github.com/template/node
fi

#docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/#peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C user -n mycc -#c '{"function":"addUser","Args":["000","admin","admin","pk","true","1","-","1"]}'

docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C user -n mycc -c '{"function":"addUser","Args":["c01","creator","admin","pk","true","3","admin","1"]}'

sleep 5

docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C user -n mycc -c '{"function":"addUser","Args":["a02","approver","admin","pk","true","2","admin","1"]}'

sleep 5

docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C user -n mycc -c '{"function":"addUser","Args":["e03","controller","admin","pk","true","4","admin","1"]}'

sleep 5

#docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/#peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C user -n mycc -#c '{"function":"addUser","Args":["002","approver","admin","pk","true","2","Data Entry Operator","1"]}'


printf "\nUser Created \n[Data Entry :username: creator , password: admin]\n[Data Approval username: approver , password: admin] \n\n\n"

docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C template -n mycc -c '{"function":"createTemplate","Args":["TEMPLATE-2000-BS-SE","student id,student name,father name,mobile no,address",
"swe-111,introduction to computer,3+1",
"swe-212,programming fundamentals,3+1",
"swe-313,algebra and calculas,3+0",
"swe-414,basic electronics,3+1",
"swe-515,communication skills,3+0",
"swe-121,introduction to computer,3+1",
"swe-222,programming fundamentals,3+1",
"swe-323,algebra and calculas,3+0",
"swe-424,basic electronics,3+1",
"swe-525,communication skills,3+0",
"swe-131,introduction to computer,3+1",
"swe-232,programming fundamentals,3+1",
"swe-333,algebra and calculas,3+0",
"swe-434,basic electronics,3+1",
"swe-535,communication skills,3+0",
"swe-141,introduction to computer,3+1",
"swe-242,programming fundamentals,3+1",
"swe-343,algebra and calculas,3+0",
"swe-444,basic electronics,3+1",
"swe-545,communication skills,3+0",
"swe-151,introduction to computer,3+1",
"swe-252,programming fundamentals,3+1",
"swe-353,algebra and calculas,3+0",
"swe-454,basic electronics,3+1",
"swe-555,communication skills,3+0",
"swe-161,introduction to computer,3+1",
"swe-262,programming fundamentals,3+1",
"swe-363,algebra and calculas,3+0",
"swe-464,basic electronics,3+1",
"swe-565,communication skills,3+0",
"swe-171,introduction to computer,3+1",
"swe-272,programming fundamentals,3+1",
"swe-373,algebra and calculas,3+0",
"swe-474,basic electronics,3+1",
"swe-575,communication skills,3+0",
"swe-181,introduction to computer,3+1",
"swe-282,programming fundamentals,3+1",
"swe-383,algebra and calculas,3+0",
"swe-484,basic electronics,3+1",
"swe-585,communication skills,3+0"
]}'

printf "\nTemplate Created ...\n\n\n"

#docker exec -e "CORE_PEER_LOCALMSPID=SSUETMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/#peerOrganizations/ssuet.example.com/users/Admin@ssuet.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C admission -n #myacc -c '{"function":"addStudent","Args":["2000-SE-001","Template Key,TEMPLATE-2000-BS-SE","student name,qasim","father name,haseeb","mobile #no,03006295847","address,House No L-350 Sector 5/c3"]}'

printf "\nStudent Added ...\n\n\n"
